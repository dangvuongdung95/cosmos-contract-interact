import { CosmWasmClient } from "cosmwasm";

import { SigningCosmWasmClient, Secp256k1HdWallet } from "cosmwasm";

const rpcEndpoint = "https://rpc.aura.network";
// https://aurascan.io/contracts/aura1un7uj5snegt52qagjsy63wpqe36n4hw2jp84df9832tfsr6r07qsxlj2eh?tabId=contract

async function main() {
  // Using a random generated mnemonic
  const mnemonic =
    "rifle same bitter control garage duck grab spare mountain doctor rubber cook";

  const wallet = await Secp256k1HdWallet.fromMnemonic(mnemonic, {
    prefix: "aura",
  });

  const addresses = await wallet.getAccounts();
  console.log(addresses);

  const client = await SigningCosmWasmClient.connectWithSigner(
    rpcEndpoint,
    wallet
  );

  const tx = await client.execute(
    addresses[0].address,
    "aura1un7uj5snegt52qagjsy63wpqe36n4hw2jp84df9832tfsr6r07qsxlj2eh",
    {
      transfer: {
        amount: 10000000,
        recipient: "aura12pku6jshmeekleg5sfhdt50a4sxjlrhuqw2rku",
      },
    }
  );

  console.log(tx);
}

main();

async function queryContract() {
  const client = await CosmWasmClient.connect(rpcEndpoint);

  const getContract = await client.getContract(
    "aura1un7uj5snegt52qagjsy63wpqe36n4hw2jp84df9832tfsr6r07qsxlj2eh"
  );
  console.log(getContract);
}

// queryContract();

async function getCount() {
  const client = await CosmWasmClient.connect(rpcEndpoint);
  const getCount = await client.queryContractSmart(
    "aura1un7uj5snegt52qagjsy63wpqe36n4hw2jp84df9832tfsr6r07qsxlj2eh",
    { token_info: {} }
  );
  console.log({ getCount });
}
// getCount();
